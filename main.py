import os
import re

import telebot
import sqlite3
import datetime
from dotenv import load_dotenv
import threading
import time

load_dotenv()

TOKEN = os.getenv("MY_TOKEN")
bot = telebot.TeleBot(TOKEN)


@bot.message_handler(commands=['help', 'start'])
def send_welcome(message):
    username = message.from_user.first_name
    lastname = message.from_user.last_name
    bot.reply_to(message, f"Привет, {username} {lastname}! Я TOOD Task Bot. Твой верный помощник в просмотре задач!")


def create_connection():
    conn = sqlite3.connect('bot_database.db')
    conn.row_factory = sqlite3.Row
    return conn


# Функция для создания таблицы каналов, если она не существует
def create_channels_table(conn):
    cursor = conn.cursor()
    cursor.execute('''CREATE TABLE IF NOT EXISTS channels
                      (id INTEGER PRIMARY KEY AUTOINCREMENT, user_id INTEGER, username TEXT, chat_id INTEGER)''')
    conn.commit()


# Функция для создания таблицы задач, если она не существует
def create_tasks_table(conn):
    cursor = conn.cursor()
    cursor.execute('''CREATE TABLE IF NOT EXISTS tasks
                      (id INTEGER PRIMARY KEY AUTOINCREMENT, user_id INTEGER, username TEXT, task_text TEXT, creation_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP, status INTEGER, chat_id INTEGER, message_id INTEGER)''')
    conn.commit()


conn = create_connection()
create_channels_table(conn)
create_tasks_table(conn)
conn.close()


def save_channel_info(user_id, username, chat_id):
    conn = create_connection()  # Создаем соединение с базой данных
    cursor = conn.cursor()
    cursor.execute('''INSERT INTO channels (user_id, username, chat_id) VALUES (?, ?, ?)''',
                   (user_id, username, chat_id))
    conn.commit()
    conn.close()  # Закрываем соединение после выполнения запроса


def get_channel_info(chat_id):
    conn = create_connection()  # Создаем соединение с базой данных
    cursor = conn.cursor()
    cursor.execute('''SELECT * FROM channels WHERE chat_id = ?''', (chat_id,))
    result = cursor.fetchone()
    conn.close()  # Закрываем соединение после получения результата
    return result


@bot.message_handler(commands=['chat'])
def get_id(message):
    if message.chat.type == 'supergroup':
        chat_id = message.chat.id
        bot.send_message(message.chat.id, f"id чата {chat_id}")


@bot.message_handler(commands=['chatid'])
def get_chat_id(message):
    if message.chat.type == 'private':  # Проверяем, что команда была отправлена в личные сообщения
        user_id = message.from_user.id
        username = message.from_user.username
        chat_id = message.chat.id
        channel_info = get_channel_info(chat_id)  # Передаем conn и chat_id
        if not channel_info:  # Если информации о чате еще нет в базе данных
            save_channel_info(user_id, username, chat_id)  # Передаем conn и другие данные
            bot.send_message(message.chat.id, f"Информация о вашем чате сохранена. Ваш chat_id: {chat_id}")
        else:
            bot.send_message(message.chat.id, f"Ваш chat_id: {chat_id}. Информация о чате уже сохранена.")
    else:
        bot.send_message(message.chat.id, "Эта команда доступна только в личных сообщениях.")


##################################################################################################################
def save_task_info(conn, user_id, username, task_text, message_id, status=0):
    creation_time = datetime.datetime.now()
    cursor = conn.cursor()
    cursor.execute(
        '''INSERT INTO tasks (user_id, username, task_text, creation_time, status, message_id) VALUES (?, ?, ?, ?, 0, ?)''',
        (user_id, username, task_text, creation_time, status, message_id))
    conn.commit()


# Функция для получения информации о задаче из базы данных
def get_task_info(conn, task_id):
    cursor = conn.cursor()
    cursor.execute('''SELECT * FROM tasks WHERE id = ?''', (task_id,))
    result = cursor.fetchone()
    return result


def get_message(message, chat_id, create_task_message_id):
    try:
        task_text = message.text  # Сохраняем текст задачи
        conn = create_connection()
        with conn:
            cursor = conn.cursor()
            cursor.execute('''SELECT username FROM channels WHERE chat_id = ?''', (chat_id,))
            result = cursor.fetchone()
            if result:
                recipient_username = result['username']

                # Сохраняем задачу в базе данных и получаем её ID
                cursor.execute('''INSERT INTO tasks (user_id, username, task_text, chat_id) VALUES (?, ?, ?, ?)''',
                               (message.from_user.id, recipient_username, task_text, message.chat.id))
                task_id = cursor.lastrowid

                # Отправляем сообщение о задаче в общий чат и сохраняем его message_id
                sent_message = bot.send_message(message.chat.id,
                                                f"#{task_id}.\nЗадача: {task_text},\nуспешно отправлена пользователю @{recipient_username}.")
                task_message_id = sent_message.message_id

                # Обновляем запись задачи в базе данных, добавляя message_id
                cursor.execute('''UPDATE tasks SET message_id = ? WHERE id = ?''', (task_message_id, task_id))
                conn.commit()

                # Удаляем сообщения между сообщениями /create_task и вводом текста
                for i in range(create_task_message_id, message.message_id+1):
                    bot.delete_message(message.chat.id, i)

                # Отправляем сообщение об успешной отправке задачи в личные сообщения
                bot.send_message(chat_id,
                                 f"Номер задачи: #{task_id}.\nВы получили новую задачу:\n{task_text}.")

            else:
                bot.send_message(message.chat.id, f"Не удалось получить информацию о пользователе.")
        conn.close()

    except telebot.apihelper.ApiException as e:
        bot.send_message(message.chat.id, f"Не удалось отправить сообщение пользователю.\nОшибка: {e}")


@bot.message_handler(commands=['create_task'])
def send_message(message):
    if message.from_user.id in [admin.user.id for admin in bot.get_chat_administrators(message.chat.id)]:
        bot.send_message(message.chat.id, "Введите имя пользователя:")
        # Сохраняем id сообщения /create_task
        bot.register_next_step_handler(message, lambda msg: get_username(msg, message.message_id))
    else:
        bot.send_message(message.chat.id, "Вы не являетесь администратором чата.")


def get_username(message, create_task_message_id):
    raw_username = message.text.strip()
    if raw_username.startswith('@'):
        username = raw_username[1:]
    else:
        username = raw_username
    conn = create_connection()
    create_tasks_table(conn)
    cursor = conn.cursor()
    cursor.execute('''SELECT * FROM channels WHERE username = ?''', (username,))
    result = cursor.fetchone()
    if result:
        # Сохраняем id сообщения, где пользователь вводит имя пользователя
        bot.send_message(message.chat.id, "Введите текст сообщения:")
        bot.register_next_step_handler(message, lambda msg: get_message(msg, result['chat_id'], create_task_message_id))
    else:
        bot.send_message(message.chat.id, "Пользователь с таким именем не найден.")
    conn.close()


####################################################
@bot.message_handler(commands=['complete_task'])
def complete_task(message):
    # Парсинг аргументов команды для получения ID задачи
    match = re.match(r'/complete_task (\d+)', message.text)
    if match:
        task_id = int(match.group(1))
        conn = create_connection()
        cursor = conn.cursor()
        cursor.execute('''SELECT * FROM tasks WHERE id = ?''', (task_id,))
        task = cursor.fetchone()
        if task:
            # Изменяем статус задачи на выполненную
            cursor.execute('''UPDATE tasks SET status = 1 WHERE id = ?''', (task_id,))
            conn.commit()

            # Перемещаем сообщение о задаче в архив
            archive_chat_id = -1002122114142  # ID вашего архивного чата
            bot.forward_message(archive_chat_id, task['chat_id'], task['message_id'])

            # Удаляем сообщения из чата и личных сообщений
            bot.delete_message(task['chat_id'], task['message_id'])
            bot.delete_message(message.chat.id, message.message_id)

            bot.send_message(message.chat.id, f"Задача #{task_id} отмечена как выполненная и перемещена в архив.")
        else:
            bot.send_message(message.chat.id, "Задача не найдена.")

        conn.close()
    else:
        bot.send_message(message.chat.id, "Неверный формат команды. Используйте /complete_task <ID_задачи>.")


########################################################################

# Функция для отправки напоминания о задаче
def remind_task(task_id, task_text, chat_id):
    current_utc_time = datetime.datetime.utcnow()
    if 0 <= current_utc_time.weekday() < 5 and 2 <= current_utc_time.hour < 9:
        bot.send_message(chat_id, f"Напоминание о задаче #{task_id}:\n{task_text}")


# Функция для проверки и отправки напоминаний
def check_reminders():
    conn = create_connection()
    cursor = conn.cursor()
    cursor.execute('''SELECT t.id, t.task_text, c.chat_id, t.username
                      FROM tasks t
                      JOIN channels c ON t.username = c.username
                      WHERE t.status IS NULL AND t.creation_time <= datetime('now', '-1 hours')''')
    tasks = cursor.fetchall()
    for task in tasks:
        task_id = task['id']
        task_text = task['task_text']
        chat_id = task['chat_id']
        remind_task(task_id, task_text, chat_id)
    conn.close()


# Функция для регулярной проверки напоминаний
def schedule_reminders():
    while True:
        check_reminders()
        # Проверяем каждый час
        time.sleep(60 * 60)


# Вызываем функцию для запуска проверки напоминаний в отдельном потоке
reminder_thread = threading.Thread(target=schedule_reminders)
reminder_thread.start()


if __name__ == "__main__":
    bot.polling(none_stop=True)
